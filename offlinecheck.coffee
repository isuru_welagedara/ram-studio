$online = $('.online')
$offline = $('.offline')

Offline.on 'confirmed-down', ->
  $online.fadeOut ->
    $offline.fadeIn()

Offline.on 'confirmed-up', ->
  $offline.fadeOut ->
    $online.fadeIn()